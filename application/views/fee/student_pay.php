<?php
$academic_year = $this->mfee->getAcademicYear(array('status' => 1));
if(!empty($academic_year))
    $academic_year_id = $academic_year[0]['id_academic_year'];
else $academic_year_id = 0;
$fee_type = $this->mfee->getClassFee(array('academic_year_id' => $academic_year_id,'class_id' => $student[0]['course_id']));
$total_fee = $this->mfee->getTotalFeeAmount(array('academic_year_id' => $academic_year_id,'class_id' => $student[0]['course_id']));
$paid_fee = $this->mfee->getTotalFeeCollected(array('academic_year_id' => $academic_year_id,'class_id' => $student[0]['course_id'],'student_id' => $student[0]['id_student']));
?>
<section class="col-lg-10 right-section">
    <ul class="breadcrumb border-btm">
        <li class="">
            <a href="<?=BASE_URL?>index.php/admin/index"> Dashboard </a>
        </li>
        <li class="active">
            Fee Pay
        </li>
    </ul>
    <div class="">
        <div class="tabs-wrapper">
            <ul id="tabs">
                <li><a href="#" name="tab1">Fee Pay</a></li>
            </ul>
            <div id="content">
                <div id="tab1">
					<form class="form-horizontal" id="fee_pay_form" method="post" action="" enctype="multipart/form-data"> 
                                         
                    <!-- Contents of Fee payment details  -->
                   
                               <div class="col-xs-12">
                                
                                <?php if($this->session->userdata('user_type_id')==ADMIN){ ?>
                                
                                <?php
                                //$fee_type_value=array(); 
                                for($s=0;$s<count($fee_type);$s++){ 
                                	//$fee_type_value[]=$fee_type[$s]['fee_type'];
                                	//$fee_type_name=strtolower($fee_type[$s]['fee_type']);
                                	$fee_type_name=str_replace(' ', '_', strtolower($fee_type[$s]['fee_type']));
                                	if($s%2==0)
                                	{
                                    ?>
                                <div class="form-group">
                                <?php } ?>
                                    <label class="col-md-3 col-xs-12 control-label"><?=$fee_type[$s]['fee_type']?></label>
                                    <div class="col-md-3 col-xs-12 m4">
                                         <input type='text' name="<?=$fee_type_name?>" id="<?=$fee_type_name?>">
                                         <span class="error" id="<?=$fee_type_name?>_error"></span>
                                    </div>
                                 <?php if($s%2==1) { ?>
                                </div>
                                <?php } } ?>
                                <?php if($s%2==1) { ?>
								</div>
								<?php } ?>
                                
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label"></label>
                                    <div class="col-md-6 col-xs-12 m4">
                                        <input type="checkbox" name="notify" id="notify"> Notify
                                        <input type="button" onclick="payAmount();" class="btn"  id="fee_pay" value="Pay">
                                        <span class="error" id="fee_err"></span>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            
                        


                   
                     </div>
                     <input type="hidden" id="student_id" value="<?=$student[0]['id_student']?>">
                        <input type="hidden" id="remain" value="<?=($total_fee[0]['amount']-$paid_fee[0]['amount'])?>">
                        <input type="hidden" id="academic_year_id" value="<?=$academic_year_id ?>">
                        <input type="hidden" id="class_id" value="<?=$student[0]['course_id'] ?>">
                        <input type='hidden' id='fee_type_value' name='fee_type_value' value='<?= json_encode($fee_type_value); ?>'>
                    <!-- End of Fee payment details  -->
                    </form>
                   
                </div>
                </div>
            
</section>
