<section class="col-lg-10 right-section">
	<ul class="breadcrumb border-btm">
        <li class="">
            <a href="<?=BASE_URL?>index.php/admin/index"> Dashboard </a>
        </li>
        <li class="active">
            Fee
        </li>
    </ul>
	<div class="col-md-12">
	<div class="col-md-12">
		<div class="row widget-wrap">
			
				<form class="form-horizontal" id="fee_history_form" method="post" action="" enctype="multipart/form-data">
                        <div class="panel-body">
                           
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Student <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12 m4">
                                    <input type="text" name="student" id="student" >
                                    <div id="student_input" class="error"></div>
                                </div>
								 <div class="col-md-3 col-xs-12 m4">
                                    <a class="btn btn-primary" id="student_search" onclick="getStudentFeePay();">Search</a>
                                </div>
                            </div>
					    </div>
                    </form>
                        <div style="width: auto;overflow: auto">
                        <table id="table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Course</th>
                                <th>Section</th>
                                <th>Admission Number</th>
                                <th>Student</th>
                                <th>Total</th>
                                <th>Paid</th>
                                <th>Due</th>
                            </tr>
                            </thead>
                            <tbody id="tbody">
                            </tbody>
                        </table>
                        </div>
				
		</div>
	</div>
</div>
	 <input type="hidden" name="selected_array" id="selected_array" value="">
</section>
<script>
    $(function() {
        $('#table').dataTable();
    });
</script>